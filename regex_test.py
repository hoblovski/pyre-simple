from regex import recompile, rematch
def test_regex():
    a = recompile("\w\w+")
    assert rematch("MaQ", a)
    assert rematch("aN", a)
    assert not rematch("a", a)
    assert not rematch("", a)
    a = recompile("\w\w*")
    assert rematch("ana", a)
    assert rematch("ae", a)
    assert rematch("a", a)
    assert not rematch("", a)
    a = recompile("\d\w?")
    assert not rematch("a_a", a)
    assert rematch("1a", a)
    assert rematch("0", a)
    assert not rematch(".", a)
    assert not rematch("", a)
    a = recompile("..\..?")
    assert not rematch("aaaa",a)
    assert rematch("aa.a",a)
    assert rematch("aa.",a)
    assert not rematch("a.",a)
    assert not rematch(".......",a)
    print("test_regex: ok")

test_regex()
