from cfg import CFG, GrammaticalAction

def test_first_follow_parse():
    V = 3
    P = [
            [0,],                     # 0
            [0, GrammaticalAction(""), "a"],         # 1
            [0, "(", GrammaticalAction(""), 2, ")"], # 2
            [1, 0],                   # 3
            [1, "*", 0],              # 4
            [2, 1],                   # 5
            [2, "+", 1, 1],           # 6
    ]
    S = 2
    cfg = CFG(V, P, S)
    cfg.compute_nullable()
    assert cfg.nullable == [True, True, True]
    cfg.compute_first()
    assert cfg.firstV == [{None, "a", "("},
            {"*", None, "a", "("},
            {"+", "*", None, "a", "("}]
    cfg.compute_follow()
    assert cfg.follow == [{"$", ")", "*", "a", "("},
            {"$", ")", "*", "a", "("},
            {"$", ")"}]
    assert not cfg.compute_LL1table("later")
    assert cfg.parse("+*a(+a*a)") == {}
    assert cfg.parse("a+") is None
    print("test_first_follow_parse: ok")

def test_attribute_grammar():
    V = 1
    P = [
            [0, GrammaticalAction("@0.val = 0")],
            [0, "a", 0, GrammaticalAction("@0.val = @2.val + 1")],
    ]
    S = 0
    cfg = CFG(V, P, S)
    cfg.prepare()
    cfg.compute_LL1table("later")
    root_attribute = cfg.parse("aaaaaaaa")
    assert root_attribute == {"val": 8}
    print("test_attribute_grammar: ok")

def test_translation_scheme():
    V = 2
    P = [
            [1, GrammaticalAction("@0.s = @0.i")],
            [1, GrammaticalAction("print(\"Hello (should be printed 2 times)\")"),
                    "a", GrammaticalAction("@2.i = @0.i + 1"), 1, GrammaticalAction("@0.s = @2.s; @0.child = @2")],
            [0, GrammaticalAction("@1.i = 0"), 1, GrammaticalAction("@0.s = @1.s; @0.child = @1")],
    ]
    S = 0
    cfg = CFG(V, P, S)
    cfg.prepare()
    cfg.compute_LL1table("later")
    root_attribute = cfg.parse("aa")
    assert root_attribute == {'s': 2,
            'child': {'s': 2, 'i': 0,
                'child': {'s': 2, 'i': 1,
                    'child': {'s': 2, 'i': 2}}}}
    print("test_translation_scheme: ok")

test_first_follow_parse()
test_attribute_grammar()
test_translation_scheme()
