from fsa import *

def test_fsa():
    a = "a"
    b = "b"
    eps = "eps"
    enfa = ENFA(range(0, 3), {a, b})
    enfa.add_trans(0, a, 1)
    enfa.add_trans(1, b, 2)
    enfa.add_trans(2, eps, 0)
    enfa.add_trans(0, a, 2)
    enfa.add_trans(0, b, 1)
    enfa.set_init_state(0)
    enfa.set_fin_states({1})
    dfa = enfa.get_dfa()
    assert dfa.accept("a")
    assert dfa.accept("b")
    assert dfa.accept("aa")
    assert dfa.accept("ab")
    assert dfa.accept("aaa")
    assert dfa.accept("aab")
    assert not dfa.accept("ba")
    assert not dfa.accept("bb")
    assert not dfa.accept("")
    print("test_fsa: ok")

test_fsa()
