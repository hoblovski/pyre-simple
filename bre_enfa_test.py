from renode import RENode
from cfg import CFG, GrammaticalAction

BRE_V = 7
BRE_P = [
        [0,     "a",                GrammaticalAction("@0.node = RENode('terminal', 'a')")],
        [0,     "b",                GrammaticalAction("@0.node = RENode('terminal', 'b')")],
        [0,     "c",                GrammaticalAction("@0.node = RENode('terminal', 'c')")],
        [0,     "d",                GrammaticalAction("@0.node = RENode('terminal', 'd')")],
        [0,     "~",                GrammaticalAction("@0.node = RENode('terminal', '')")],
        [0,     "(",    5,  ")",    GrammaticalAction("@0.node = @2.node")],
        # 0: atom

        [1,     0,      2,          GrammaticalAction("@0.node = RENode('closure', @1.node) if @2.closure else @1.node")],
        [2,                         GrammaticalAction("@0.closure = False")],
        [2,     "*",                GrammaticalAction("@0.closure = True")],
        # 1: atom* or atom

        [3,     1,      4,          GrammaticalAction("@0.node = RENode('concat', @1.node, *@2.nodelist) if @2.nodelist != [] else @1.node")],
        [4,                         GrammaticalAction("@0.nodelist = []")],
        [4,     1,      4,          GrammaticalAction("@0.nodelist = [@1.node] + @2.nodelist")],
        # 3: 1 1 1...

        [5,     3,      6,          GrammaticalAction("@0.node = RENode('oneof', @1.node, *@2.nodelist)")],
        [6,                         GrammaticalAction("@0.nodelist = []")],
        [6,     "+",    3,      6,  GrammaticalAction("@0.nodelist = [@2.node] + @3.nodelist")],
        # 5: 3 + 3 + 3...
]
BRE_S = 5

def test_bre():
    cfg = CFG(BRE_V, BRE_P, BRE_S)
    cfg.prepare()
    cfg.compute_LL1table()
    S = "abc(a*d+c)+~+c"
    node = cfg.parse(S, cfg.from_import("renode", "RENode"))
    node = node["node"]
    enfa = node.getENFA(set("abcd"))
    enfa = enfa[1]
    dfa = enfa.get_dfa()
    assert dfa.accept("c")
    assert dfa.accept("")
    assert dfa.accept("abcc")
    assert dfa.accept("abcd")
    assert dfa.accept("abcad")
    assert dfa.accept("abcaad")
    assert dfa.accept("abc"+ "a"*100 + "d")
    assert not dfa.accept("abcda")
    assert not dfa.accept("abc")
    assert not dfa.accept("d")
    assert not dfa.accept("ab")
    assert not dfa.accept("abd")
    print("test_bre: ok")

test_bre()
