from fsa import ENFA
from common import *

class RENode:
    _ALLOWED_TYPES = {"oneof", "closure",
            "concat", "terminal", "postv-closure", "one-or-zero"}

    def __init__(self, typ, *children):
        assert typ in RENode._ALLOWED_TYPES
        if typ == "oneof":
            assert len(children) >= 1
            for c in children:
                assert type(c) == RENode
            self.typ = typ
            self.oprnds = children

        elif typ == "closure" or typ == "postv-closure" or typ == "one-or-zero":
            assert len(children) == 1
            assert type(children[0]) == RENode
            self.typ = typ
            self.oprnd = children[0]

        elif typ == "concat":
            assert len(children) >= 1
            for c in children:
                assert type(c) == RENode
            self.typ = typ
            self.oprnds = children

        elif typ == "terminal":
            assert len(children) == 1
            assert type(children[0]) == str
            self.typ = typ
            self.oprnd = children[0]

        else:
            assert False

    def dump(self, indent=0):
        if self.typ in {"oneof", "concat"}:
            print(" "*indent, "<%s>" % self.typ)
            for c in self.oprnds:
                c.dump(indent+4)
        elif self.typ in {"closure"}:
            print(" "*indent, "<%s>" % self.typ)
            self.oprnd.dump(indent+4)
        elif self.typ in {"terminal"}:
            print(" "*indent, "<%s>" % self.typ)
            if len(self.oprnd) != 0:
                print(" "*(indent+4), self.oprnd)
            else:
                print(" "*(indent+4), "<empty string>")
        else:
            assert False

    def getENFA(self, sigma, begin_range=0):
        """
            @returns: (end_range, enfa)
                enfa: enfa.fin_states guaranteed only 1 element
        """

        if self.typ == "terminal":
            term_str = self.oprnd
            s0 = begin_range
            sf = begin_range + len(term_str)
            states = range(s0, sf + 1)
            enfa = ENFA(states, sigma)
            enfa.set_init_state(s0)
            for i, s in enumerate(states[:-1]):
                enfa.add_trans(s, term_str[i], s+1)
            enfa.set_fin_states({sf})
            return sf + 1, enfa

        elif self.typ == "postv-closure":
            begin_range, enfa = self.oprnd.getENFA(sigma, begin_range)
            s0 = begin_range
            sf = begin_range + 1 
            enfa.add_state({s0, sf})
            enfa.add_trans(s0, "eps", enfa.init_state)
            enfa.add_trans(setsel(enfa.fin_states), "eps", sf)
            enfa.add_trans(setsel(enfa.fin_states), "eps", enfa.init_state)
            enfa.set_init_state(s0)
            enfa.set_fin_states({sf})
            return sf + 1, enfa

        elif self.typ == "one-or-zero":
            begin_range, enfa = self.oprnd.getENFA(sigma, begin_range)
            s0 = begin_range
            sf = begin_range + 1 
            enfa.add_state({s0, sf})
            enfa.add_trans(s0, "eps", enfa.init_state)
            enfa.add_trans(setsel(enfa.fin_states), "eps", sf)
            enfa.add_trans(s0, "eps", sf)
            enfa.set_init_state(s0)
            enfa.set_fin_states({sf})
            return sf + 1, enfa

        elif self.typ == "closure":
            begin_range, enfa = self.oprnd.getENFA(sigma, begin_range)
            s0 = begin_range
            sf = begin_range + 1 
            enfa.add_state({s0, sf})
            enfa.add_trans(s0, "eps", enfa.init_state)
            enfa.add_trans(setsel(enfa.fin_states), "eps", sf)
            enfa.add_trans(s0, "eps", sf)
            enfa.add_trans(setsel(enfa.fin_states), "eps", enfa.init_state)
            enfa.set_init_state(s0)
            enfa.set_fin_states({sf})
            return sf + 1, enfa

        elif self.typ == "concat":
            begin_range, enfa = self.oprnds[0].getENFA(sigma, begin_range)
            for cself in self.oprnds[1:]:        
                end_range, new_enfa = cself.getENFA(sigma, begin_range)
                enfa.add_state(new_enfa.states)
                for ns in new_enfa.states:
                    enfa.trans[ns] = new_enfa.trans[ns]
                enfa.add_trans(setsel(enfa.fin_states),
                        "eps", new_enfa.init_state)
                enfa.set_fin_states(new_enfa.fin_states)
                begin_range = end_range
            return begin_range, enfa

        elif self.typ == "oneof":
            s0 = begin_range
            sf = begin_range + 1
            enfa = ENFA({s0, sf}, sigma)
            enfa.set_init_state(s0)
            enfa.set_fin_states({sf})
            begin_range += 2
            for cself in self.oprnds:
                end_range, new_enfa = cself.getENFA(sigma, begin_range)
                enfa.add_state(new_enfa.states)
                for ns in new_enfa.states:
                    enfa.trans[ns] = new_enfa.trans[ns]
                enfa.add_trans(s0, "eps", new_enfa.init_state)
                enfa.add_trans(setsel(new_enfa.fin_states), "eps", sf)
                begin_range = end_range
            return begin_range, enfa

        else:
            assert False
