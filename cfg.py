"""Recursive Descent
"""

from common import *
import importlib



class GrammaticalAction:
    def __init__(self, action):
        self.action = action

    def __str__(self):
        return str(self.action)


class CFG:
    def __init__(self, V_size, init_P, S):
        """
            @param v_size:
                Integers in 0..V_size-1 represent nonterminals.
            @param init_P:
                A list of productions.
                Each entry p is also a list,
                    p[0] is lhs, should be nonterminal 
                    p[1:] is rhs, either terminal, nonterminal or action
                L-translation mode required that actions do not 
                    redefine attributes already defined before,
                    and do not reference attributes not defined yet.
            @param S:
                The start nonterminal
        """
        assert type(V_size) == int
        assert type(init_P) == list
        assert S >= 0 and S < V_size
        self._check_P(V_size, init_P)
        self._V = V_size
        self._P = init_P
        self._S = S

    def is_terminal(self, X):
        """X is any element in a production"""
        return type(X) == str
    def is_nonterminal(self, X):
        """X is any element in a production"""
        return type(X) == int
    def is_action(self, X):
        """X is any element in a production"""
        return type(X) == GrammaticalAction
    def is_innullable(self, X):
        return self.is_terminal(X) or\
                (self.is_nonterminal(X) and not self.nullable[X])
    def _first(self, alpha):
        """First(alpha), based on First(A)"""
        rv = set()
        for X in alpha:
            if type(X) == str:
                return rv | {X}
            if type(X) == int:
                rv |= self.firstV[X]
                if not self.nullable[X]:
                    return rv
        return rv | {None}


    def compute_nullable(self):
        """The most simple iteration algorithm
            @result: self.nullable[A], 0 <= A < self._V
        """
        self.nullable = [False for i in range(self._V)]
        change = True
        while change:
            change = False
            for p in self._P:
                rhs_nullable = not any(self.is_innullable(X) for X in p[1:])
                if rhs_nullable and not self.nullable[p[0]]:
                    self.nullable[p[0]] = True
                    change = True

    def compute_first(self):
        """
            @result: firstV[A], 0 <= A < self._V;
                     firstP[p], 0 <= p < len(self._P)
        """
        # firstV[A]: first(A)
        self.firstV = [{None} if self.nullable[i] else set()
                for i in range(self._V)]
        change = True
        while change:
            change = False
            for p in self._P:
                A = p[0]
                for X in p[1:]:
                    if self.is_terminal(X):
                        if X not in self.firstV[A]:
                            self.firstV[A] |= {X}
                            change = True
                        break
                    elif self.is_nonterminal(X):
                        d = self.firstV[X] - self.firstV[A]
                        if len(d) != 0:
                            self.firstV[A] |= d
                            change = True
                        if not self.nullable[X]:
                            break
        # firstP[i]: first(rhs(P[i]))
        self.firstP = [self._first(p[1:]) for p in self._P]

    def compute_follow(self):
        """
            @result: self.follow[A], 0 <= A < len(self._P)
        """
        # follow[A]: follow(A)
        self.follow = [{'$'} if i == self._S else set()
                for i in range(self._V)]
        change = True
        while change:
            change = False
            for p in self._P:
                for i in range(1, len(p)):
                    X = p[i]
                    if not self.is_nonterminal(X):
                        continue
                    for j in range(i+1, len(p)):
                        Y = p[j]
                        if self.is_terminal(Y):
                            if Y not in self.follow[X]:
                                self.follow[X] |= {Y}
                                change = True
                            break
                        elif self.is_nonterminal(Y):
                            d = self.firstV[Y] - self.follow[X] - {None}
                            if len(d) != 0:
                                self.follow[X] |= d
                                change = True
                            if not self.nullable[Y]:
                                break
                    else:
                        d = self.follow[p[0]] - self.follow[X]
                        if len(d) != 0:
                            self.follow[X] |= d
                            change = True

    def compute_LL1table(self, conflict_resolve="later"):
        """
            @param conflict_resolve: "earlier" | "later"
                it determines the precedence of productions
                    when self is not an LL(1) grammar
            @returns:
                False if this CFG is not LL(1)
                (for this situation,
                 production precedence is determined by
                 order of occurrence)
        """
        assert conflict_resolve in {"earlier", "later"}
        self.ll1tbl = [{} for i in range(self._V)]
        is_ll1 = True
        for i, pi in enumerate(self._P):
            predi = self.firstP[i]
            if None in predi:
                predi = (predi | self.follow[pi[0]]) - {None}
            for a in predi:
                if a in self.ll1tbl[pi[0]]:
                    is_ll1 = False
                    if conflict_resolve == "later":
                        self.ll1tbl[pi[0]][a] = i   # override with later
                else:
                    self.ll1tbl[pi[0]][a] = i
        return is_ll1

    def prepare(self):
        """Just a shorthand"""
        self.compute_nullable()
        self.compute_first()
        self.compute_follow()

    def _parse(self, A, w, offset, infoA):
        """Parse given input with the algorithm of recursive descent.

            @param A: int, A \in V.
                The non-terminal we are parsing
            @param w: str
            @param offset: int, 0 <= offset < len(w)
                Together specifies the input string w[offset:]
            @param infoA: dict<str, object>
                Inherited attributes of A

            @returns: (offset, infoA), or None
                If None is returned, check the translation scheme.
                offset: int
                    Where we have progressed after parsing the non-terminal A,
                        either successfully or aborted.
                infoA:  dict<str, anytype>, or None
                    Attributes of A (including inherited and synthesised).
                    If None is returned, the input w is not in current CFG.
        """
        rhs = []
        try:
            rhs = self._P[self.ll1tbl[A][w[offset]]][1:]
        except KeyError:
            # no ll1 table entry found for (A, lookahead)
            return offset, None
        # attributes of each element in the production
        #   don't use list multiplication here, you know
        attr_X = [infoA] + [{} for i in range(len(rhs))]
        # used for attr_X counter (does not account for actions)
        idx_token = 1
        for i, X in enumerate(rhs):
            if self.is_terminal(X):
                # match token
                if not w[offset:].startswith(X):
                    # input does not match ll1 table
                    return offset, None
                offset += len(X)
                idx_token += 1
            elif self.is_nonterminal(X):
                offset, attr_X[idx_token] = self._parse(X, w, offset, attr_X[idx_token])
                if attr_X[idx_token] is None:
                    return offset, None
                idx_token += 1
            elif self.is_action(X):
                # only allow @i.a = func(@j.b)
                action = self._translate(X.action)
                exec(action)
            else:
                # never here
                assert False
        return offset, attr_X[0]

    def parse(self, w, pre_execute=""):
        """
            @returns: None if w is not in CFG,
                or the attributes of start token if parsing succeeded
        """
        exec(pre_execute)
        w = w + "$" # add end-of-input explicitly
        t, root_attribute = self._parse(self._S, w, 0, {})
        if (t != len(w) - 1):
            root_attribute = None
        return root_attribute

#*****************************************************************************
# ugliness starts from here

    def _translate(self, inst):
        # look for `@i.a` convert to `attr_X[i][a]
        # now also allows @i
        rv = ""
        p = 0
        while p < len(inst):
            # I am not allowed to use regex. write my own FSM
            while p < len(inst) and inst[p] != '@':
                rv = rv + inst[p]
                p += 1
            if p >= len(inst):
                break
            # found @ before inst ends
            rv = rv + "attr_X["
            p += 1
            assert p < len(inst) # prevent malformed instruction
            # look for \d+
            while p < len(inst) and inst[p] in DIGIT:
                rv = rv + inst[p]
                p += 1
            assert p <= len(inst) # prevent malformed instruction
            if p >= len(inst) or inst[p] != '.':
                rv = rv + "]"
                # @\d+
                continue
            # @\d+.
            rv = rv + "][\""
            p += 1
            assert p < len(inst) # prevent malformed instruction
            # look for \w+
            assert inst[p] in IDSTART
            rv = rv + inst[p]
            p += 1
            assert p <= len(inst) # prevent malformed instruction
            while p < len(inst) and inst[p] in IDMIDDLE:
                rv = rv + inst[p]
                p += 1
            assert p <= len(inst) # prevent malformed instruction
            rv = rv + "\"]"
        return rv

    def _check_P(self, V, P):
        """
            This is only partial check.
            The best way to avoid mistake is make grammars correctly L-translation.
        """
        Vs = range(V) # avoid re-evaluation in loops
        for p in P:
            assert p[0] in Vs
            for X in p[1:]:
                assert self.is_nonterminal(X) or self.is_terminal(X)\
                        or self.is_action(X)

    def from_import(self, pkg, *import_param):
        rv = ""
        import_names = []
        for s in import_param:
            if type(s) == str:
                import_names += [s]
            elif type(s) == tuple:
                import_names += [s[0]]
            else:
                assert False
        rv += ("_temp__ = __import__(%s, globals(), locals(), %s)\n" % (\
                "'" + pkg + "'",\
                str(import_names)))
        for iname in import_param:
            if type(iname) == str:
                # from xxx import xx
                rv += ("global %s\n" % iname)
                rv += ("%s = _temp__.%s\n" % (iname, iname))
            elif type(iname) == tuple:
                # from xxx import xx as x
                rv += ("global %s\n" % iname[1])
                rv += ("%s = _temp__.%s\n" % (iname[1], iname[0]))
            else:
                assert False
        return rv
