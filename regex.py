from common import *
from cfg import CFG, GrammaticalAction as Action

#*****************************************************************************
# Basic RE CFG definition
BRE_V = 8
BRE_P = [
        [0,     "\\",   7,          Action("@0.node = RENode('terminal', @2.char)")],
        [0,     "~",                Action("@0.node = RENode('terminal', '')")],
        [0,     "(",    5,  ")",    Action("@0.node = @2.node")],
        # 0: atom

        [1,     0,      2,          Action("@0.node = RENode(@2.repeat, @1.node) if @2.repeat is not None else @1.node")],
        [2,                         Action("@0.repeat = None")],
        [2,     "*",                Action("@0.repeat = 'closure'")],
        [2,     "+",                Action("@0.repeat = 'postv-closure'")],
        [2,     "?",                Action("@0.repeat = 'one-or-zero'")],
        # 1: atom* or atom

        [3,     1,      4,          Action("@0.node = RENode('concat', @1.node, *@2.nodelist) if @2.nodelist != [] else @1.node")],
        [4,                         Action("@0.nodelist = []")],
        [4,     1,      4,          Action("@0.nodelist = [@1.node] + @2.nodelist")],
        # 3: 1 1 1...

        [5,     3,      6,          Action("@0.node = RENode('oneof', @1.node, *@2.nodelist)")],
        [6,                         Action("@0.nodelist = []")],
        [6,     "|",    3,      6,  Action("@0.nodelist = [@2.node] + @3.nodelist")],
        # 5: 3 + 3 + 3...
]
ESCAPE_CHARS = set(r"\~*|()$+?")
for i in ALLOWED_CHARS - ESCAPE_CHARS:
    BRE_P = [[0,     i,                Action("@0.node = RENode('terminal', %s)"%repr(i))]] + BRE_P
for i in ESCAPE_CHARS:
    BRE_P = [[7,     i,                Action("@0.char = %s"%repr(i)) ]] + BRE_P
BRE_S = 5

#*****************************************************************************
# definition for \x, x is any char
replace= {}
def _oneof(chars):
    chars = list(chars)
    for i in range(len(chars)):
        if chars[i] in ESCAPE_CHARS:
            chars[i] = "\\"+chars[i]
    return "(" + "|".join(chars) + ")"
_u_SET = set("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
_l_SET = set("abcdefghijklmnopqrstuvwxyz")
_w_SET = _u_SET | _l_SET | {'_'}
_d_SET = set("0123456789")
_x_SET = _d_SET | set("abcdefABCDEF")

replace[r"\w"] = _oneof(_w_SET)
replace[r"\W"] = _oneof(VISIBLE_CHARS - _w_SET)
replace[r"\d"] = _oneof(_d_SET)
replace[r"\D"] = _oneof(VISIBLE_CHARS - _d_SET)
replace[r"\u"] = _oneof(_u_SET)
replace[r"\U"] = _oneof(VISIBLE_CHARS - _u_SET)
replace[r"\l"] = _oneof(_l_SET)
replace[r"\L"] = _oneof(VISIBLE_CHARS - _l_SET)
replace[r"\x"] = _oneof(_x_SET)

_dot_repr = _oneof(VISIBLE_CHARS)

#*****************************************************************************
def replace_regex_macros(regex):
    for x in replace:
        regex = regex.replace(x, replace[x])
    i = 0
    while i < len(regex):
        if regex[i] == '.':
            if (i == 0 or regex[i-1] != '\\'):
                # dot matching all
                regex = regex[:i] + _dot_repr + ("" if i == len(regex)-1 else regex[i+1:])
                i += len(_dot_repr)
            else:
                # escaped dot
                regex = regex[:i-1] + "." + ("" if i == len(regex)-1 else regex[i+1:])
        else:
            i += 1
    return regex

#*****************************************************************************
cfg = CFG(BRE_V, BRE_P, BRE_S)
cfg.prepare()
isll1 = cfg.compute_LL1table("earlier")
assert isll1

def recompile(pattern):
    """
        @returns: Matcher
    """
    pattern = replace_regex_macros(pattern)
    node = cfg.parse(pattern, cfg.from_import("renode", "RENode"))["node"]
    enfa = node.getENFA(ALLOWED_CHARS)[1]
    dfa = enfa.get_dfa() # bottleneck
    return dfa

def rematch(text, matcher):
    return matcher.accept(text)
