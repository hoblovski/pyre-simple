"""commonly used constants / functions
"""
DIGIT    = frozenset("0123456789")
HEXDIGIT = frozenset("0123456789abcdefABCDEF")

# IDSTART := [A-Za-z_]
IDSTART  = frozenset([chr(i) for i in range(ord('a'), ord('z')+1)]\
        + [chr(i) for i in range(ord('A'), ord('Z')+1)]\
        + ['_'])
# IDMIDDLE := [A-Za-z0-9_]
IDMIDDLE = IDSTART |\
        frozenset([chr(i) for i in range(ord('0'), ord('9')+1)])

VISIBLE_CHARS = frozenset([chr(i) for i in range(0x20, 0x7f)]) | {'\t'}
ALLOWED_CHARS = VISIBLE_CHARS | {'\n'}


BLANK = frozenset(" \t\n")
def isblank(c):
    return c in BLANK

def setsel(s):
    # avoid editing a set while looping over it
    assert type(s) == set or type(s) == frozenset
    if len(s) == 0:
        return None
    else:
        for i in s:
            return i

