from common import *

def _empty(i):
    return len(i) == 0

class DFA:
    def __init__(self, states, sigma, trans,\
            init_state, fin_states):
        assert type(trans) == dict
        assert frozenset(trans.keys()) == states
        for ns in states:
            assert frozenset(trans[ns].keys()) == sigma
            for a in sigma:
                assert trans[ns][a] in states
        # trans has been validated
        #
        assert init_state in states
        assert fin_states <= states
        self.states = frozenset(states)
        self.sigma = frozenset(sigma)
        self.trans = trans
        self.init_state = init_state
        self.fin_states = frozenset(fin_states)

    def accept(self, w):
        s = self.init_state
        for a in w:
            assert a in self.sigma
            s = self.trans[s][a]
        return s in self.fin_states

    def dump(self):
        print("******* Dumping DFA")
        print(">>> states")
        for s in self.states:
            print("|  ", s)
        print(">>> sigma")
        print(self.sigma)
        print(">>> init state")
        print(self.init_state)
        print(">>> fin states")
        for s in self.fin_states:
            print("|  ", s)
        print(">>> trans")
        i = 0
        for s in self.states:
            for a in self.sigma:
                print("|  ",\
                        str(s).rjust(20, " "),\
                        str(a).rjust(5, " "),\
                        " -> ",\
                        self.trans[s][a])
                i += 1
                if i % 4 == 0:
                    print("|")
        print("******* Finished")



class ENFA:
    def __init__(self, states, sigma):
        states = set(states)
        sigma = frozenset(sigma)
        assert "eps" not in sigma 
        self.states = states
        self.sigma = sigma
        self.trans = dict()
        for s in states:
            self.trans[s] = dict()
            self.trans[s]["eps"] = set()
            for a in sigma:
                self.trans[s][a] = set()
        self.init_state = None
        self.fin_states = None

    def add_trans(self, s, a, s1):
        """ delta[s, a] -> s1
            a in sigma or a == "eps"
            @returns:
                True if success;
                False if already exists
        """
        assert s in self.states
        assert s1 in self.states
        assert (a == "eps") or (a in self.sigma)
        if s1 in self.trans[s][a]:
            return False
        else:
            self.trans[s][a] |= {s1}
            return True

    def add_state(self, ss):
        assert len(ss & self.states) == 0
        self.states |= ss
        for s in ss:
            self.trans[s] = dict()
            self.trans[s]["eps"] = set()
            for a in self.sigma:
                self.trans[s][a] = set()

    def set_init_state(self, s0):
        assert s0 in self.states
        self.init_state = s0

    def set_fin_states(self, ss):
        ss = frozenset(ss)
        assert ss <= self.states
        self.fin_states = ss

    def dump(self):
        print("******* Dumping ENFA")
        print("states:", self.states)
        #print("sigma:", self.sigma)
        for s in self.states:
            for a in self.sigma | {"eps"}:
                if self.trans[s][a] != set():
                    print("  ", str(s).ljust(5, " "),\
                            str(a).ljust(5, " "), " -> ",\
                            self.trans[s][a])
        print("init_state:", self.init_state)
        print("fin_states:", self.fin_states)
        print("******* Finish")

    def _etrans(self, ss, a):
        """ extended transition function
            @param ss: state subset
        """
        assert type(ss) == frozenset
        assert ss <= self.states
        assert (a == "eps") or (a in self.sigma)
        rv = set()
        for s in ss:
            rv |= self.trans[s][a]
        return frozenset(rv)

    def _eclose(self, ss):
        ss = frozenset(ss)
        while True:
            ss_t = ss | self._etrans(ss, "eps")
            if ss_t == ss:
                break
            else:
                ss = ss_t
        return ss

    def get_dfa(self):
        """Get DFA equivalent to this ENFA, using subset construction.
            @returns: for DFA,
                states,
                sigma,
                trans,
                init state,
                fin state
        """
        assert self.init_state is not None
        assert self.fin_states is not None
        nistate = self._eclose({self.init_state}) # nfa init state
        # till now we get nistate right
        #
        nstates = [nistate] # nfa states, numbered
        # not expanded yet, subset of nstates
        nstates_open = {nistate}
        ntrans = {}
        while not _empty(nstates_open):
            ns = setsel(nstates_open)
            nstates_open -= {ns}
            ntrans[ns] = dict()
            for a in self.sigma:
                ns1 = self._eclose(self._etrans(ns, a))
                ntrans[ns][a] = ns1
                if ns1 not in nstates:
                    nstates += [ns1]
                    nstates_open |= {ns1}
        nstates = frozenset(nstates)
        # till now we get nstates and ntrans right
        #
        nfstates = set() # nfa fin states
        for ns in nstates:
            if not _empty(ns & self.fin_states):
                nfstates |= {ns}
        nfstates = frozenset(nfstates)
        # till now we get nfstates right
        #
        rv = DFA(nstates, self.sigma, ntrans, nistate, nfstates)
        return rv

