from renode import RENode
from common import *

def test_terminal():
    node = RENode("terminal", "good")
    s, enfa = node.getENFA(IDMIDDLE)
    assert s == 4 + 1
    dfa = enfa.get_dfa()
    assert dfa.accept("good")
    assert not dfa.accept("goo")
    assert not dfa.accept("")
    assert not dfa.accept("god")
    assert not dfa.accept("goodg")
    print("test_terminal: ok")

def test_closure():
    node = RENode("terminal", "good")
    node = RENode("closure", node)
    s, enfa = node.getENFA(IDMIDDLE)
    assert s == 4 + 1 + 2
    dfa = enfa.get_dfa()
    assert not dfa.accept("goiod")
    assert not dfa.accept("go")
    assert not dfa.accept("g")
    assert not dfa.accept("goods")
    assert not dfa.accept("goodgg")
    assert not dfa.accept("goodgoo")
    assert not dfa.accept("goodgoodg")
    assert dfa.accept("good")
    assert dfa.accept("goodgood")
    assert dfa.accept("goodgoodgood")
    assert dfa.accept("good")
    assert dfa.accept("good")
    assert dfa.accept("")
    print("test_closure: ok")

def test_postvclosure():
    node = RENode("terminal", "good")
    node = RENode("postv-closure", node)
    s, enfa = node.getENFA(IDMIDDLE)
    assert s == 4 + 1 + 2
    dfa = enfa.get_dfa()
    assert not dfa.accept("goiod")
    assert not dfa.accept("go")
    assert not dfa.accept("g")
    assert not dfa.accept("goods")
    assert not dfa.accept("goodgg")
    assert not dfa.accept("goodgoo")
    assert not dfa.accept("goodgoodg")
    assert dfa.accept("good")
    assert dfa.accept("goodgood")
    assert dfa.accept("goodgoodgood")
    assert dfa.accept("good")
    assert dfa.accept("good")
    assert not dfa.accept("")
    print("test_postvclosure: ok")

def test_oneorzero():
    node = RENode("terminal", "good")
    node = RENode("one-or-zero", node)
    s, enfa = node.getENFA(IDMIDDLE)
    assert s == 4 + 1 + 2
    dfa = enfa.get_dfa()
    assert not dfa.accept("goiod")
    assert not dfa.accept("go")
    assert not dfa.accept("g")
    assert not dfa.accept("goods")
    assert not dfa.accept("goodgg")
    assert not dfa.accept("goodgoo")
    assert not dfa.accept("goodgoodg")
    assert dfa.accept("good")
    assert dfa.accept("")
    assert not dfa.accept("goodgood")
    assert not dfa.accept("goodgoodgood")
    print("test_oneorzero: ok")

def test_eps():
    node = RENode("terminal", "")
    s, enfa = node.getENFA(IDMIDDLE)
    assert s == 1
    dfa = enfa.get_dfa()
    assert not dfa.accept("g")
    assert not dfa.accept("o")
    assert not dfa.accept("good")
    assert not dfa.accept("goodgood")
    assert dfa.accept("")
    print("test_eps: ok")

def test_concat():
    node = RENode("concat",
            RENode("terminal", "go"),
            RENode("terminal", "o"),
            RENode("terminal", "d"))
    s, enfa = node.getENFA(IDMIDDLE)
    dfa = enfa.get_dfa()
    assert dfa.accept("good")
    assert not dfa.accept("goo")
    assert not dfa.accept("")
    assert not dfa.accept("god")
    assert not dfa.accept("goodg")
    print("test_concat: ok")

def test_oneof():
    node = RENode("oneof",
            RENode("terminal", "g"),
            RENode("terminal", "go"),
            RENode("terminal", "goo"),
            RENode("terminal", "good"),
            RENode("terminal", "god"))
    s, enfa = node.getENFA(IDMIDDLE)
    dfa = enfa.get_dfa()
    assert dfa.accept("g")
    assert dfa.accept("go")
    assert dfa.accept("goo")
    assert dfa.accept("good")
    assert dfa.accept("god")
    assert not dfa.accept("godo")
    assert not dfa.accept("goodd")
    assert not dfa.accept("gdo")
    assert not dfa.accept("goood")
    print("test_oneof: ok")

test_terminal()
test_closure()
test_postvclosure()
test_oneorzero()
test_eps()
test_concat()
test_oneof()

